# WEB Products

> Test application for FCamara

## Build Setup
* npm must be installed

``` bash
# go to project folder
cd .../products-web/

# install dependencies
npm install

# serve with hot reload at localhost:8088
npm run dev

# build for production with minification (optional)
npm run build

````

Login with **user** and **1234**