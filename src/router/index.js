import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Products from '@/components/Products'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/products/',
      component: Products
    },
    {
      path: '/',
      component: Login
    }
  ]
})
